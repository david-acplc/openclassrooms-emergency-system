package com.openclassrooms.emergencysystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmergencySystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmergencySystemApplication.class, args);
    }

}
